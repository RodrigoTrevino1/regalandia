$("#zip").keyup(function() {
    var el = $(this);

    if ((el.val().length == 5) && (is_int(el.val()))) {
        // console.log(el.val());
        //url = "http://api.zippopotam.us/mx/" + el.val();
        url = "https://api-sepomex.hckdrk.mx/query/info_cp/" + el.val();
        // console.log(url);
        // Make Ajax call, etc
        $.ajax({
            url: url,
            cache: false,
            dataType: "json",
            type: "GET",
            success: function(result, success) {

                // $(".fancy-form div > div").slideDown(); /* Show the fields */

                // places = result['places'][0];
                // $("#city").val(places['place name']);
                // $("#state").val(places['state']);
                place = result[0].response;
                $("#city").val(place.ciudad);
                $("#state").val(place.estado);
                // console.log(result[0].response);
                $(".zip-error").hide(); /* In case they failed once before */

                // $("#address-line-1").focus(); /* Put cursor where they need it */
                // console.log(result.places[0]);
                // console.log(result.places[0].place_name);
                // console.log(result.places[0].state);

            },
            error: function(result, success) {

                $(".zip-error").show(); /* Ruh row */

            }

        });

    }

})

function is_int(value) {
    if ((parseFloat(value) == parseInt(value)) && !isNaN(value)) {
        return true;
    } else {
        return false;
    }
}