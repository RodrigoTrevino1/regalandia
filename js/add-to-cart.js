$(document).ready(function() {
  var count = 0;
  $("button.add-to-cart").click(function(event) {
    count++;
    var button = this;
    var carrito = $(this).closest('div').children('.cart')
    $(carrito).css("display", "block");
    $(button).addClass("size");
    setTimeout(function() {
      $(button).addClass("hover");
    }, 200);
    setTimeout(function() {
      $("div.icon.cart > span").addClass("counter");
      $("div.icon.cart > span.counter").text(count);
    }, 400);
    setTimeout(function() {
      $(button).removeClass("hover");
      $(button).removeClass("size");
      $(carrito).css("display", "none");
    }, 600);
    event.preventDefault();
  });
});
